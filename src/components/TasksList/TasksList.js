import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {deleteTask, fetchTask} from "../../store/action";
import Task from "../../components/Task/Task";

const TasksList = () => {
    const toDo = useSelector(state => state.toDo);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchTask());
    }, [dispatch]);

    const deleteTaskFromList = (id) => {
       dispatch(deleteTask(id));
    };

    const dataTask = Object.keys(toDo).map((task) => {
        return (
            <Task
                id={task}
                key={task}
                toDo={toDo[task].task}
                onClick={() => deleteTaskFromList(task)}
            />
        );
    });

    return (
        <div>
            {dataTask}
        </div>
    );
};

export default TasksList;