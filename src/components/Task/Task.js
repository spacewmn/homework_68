import React from "react";

const Task = props => {
    return (
    <div className="card mb-4">
        <div className="card-body">
            <p id={props.id} className="card-text">{props.toDo}</p>
            <button className="btn btn-danger float-right" type="button" onClick={props.onClick}>delete</button>
        </div>
    </div>
    );
};

export default Task;