import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {postTask} from "../../store/action";

const ToDoes = () => {
    const dispatch = useDispatch();

    const [newTasks, setNewTasks] = useState({
       task: ''
    });

    const currentTask = (event) => {
            setNewTasks(event.target.value);
    };

    const onClickPost = (e) => {
            e.preventDefault();
            dispatch(postTask(newTasks));
    };

    return (
        <div>
            <form className="task-form d-block" onSubmit={onClickPost}>
                <div className="input-group mb-3">
                    <input
                        type="text"
                        name="task"
                        className="form-control d-inline mr-3"
                        onChange={currentTask}
                        required
                    />
                    <button type="submit" className="btn btn-success px-5">Add</button>
                </div>
            </form>
        </div>
    );
};

export default ToDoes;