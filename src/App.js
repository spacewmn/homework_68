import React from 'react';
import ToDoes from "./containers/ToDoes/ToDoes";
import TasksList from "./components/TasksList/TasksList";

const App = () => {
 return (
     <div className="container my-5">
          <ToDoes/>
          <TasksList/>
     </div>
 );
};

export default App;