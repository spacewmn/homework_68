import instance from "../axios-toDoes";
import {
    DELETE_ERROR,
    DELETE_REQUEST,
    DELETE_SUCCESS,
    FETCH_ERROR,
    FETCH_REQUEST,
    FETCH_SUCCESS,
    POST_ERROR
} from "./actionTypes";

export const fetchRequest = () => (
    {type: FETCH_REQUEST}
);

export const fetchSuccess = task => (
    {type: FETCH_SUCCESS, task}
);

export const fetchError = () => (
    {type: FETCH_ERROR}
);

export const fetchTask = () => {
    return async dispatch => {
        dispatch(fetchRequest());
        try {
            const response = await instance.get('/task.json');
            dispatch(fetchSuccess(response.data));
        } catch (e) {
            dispatch(fetchError(e));
        }
    };
};

export const postError = () => (
    {type: POST_ERROR}
);

export const postTask = (task) => {
    return async dispatch => {
        try {
            await instance.post('/task.json', {task: task});
            dispatch(fetchTask());
        } catch (e) {
            dispatch(postError(e));
        }
    }
};

export const deleteRequest = () => (
    {type: DELETE_REQUEST}
);

export const deleteSuccess = id => (
    {type: DELETE_SUCCESS, id}
);

export const deleteError = () => (
    {type: DELETE_ERROR}
);

export const deleteTask = (id) => {
    return async dispatch => {
        dispatch(deleteRequest());
        try {
            const response = await instance.delete('/task/' + id + '.json');
            dispatch(deleteSuccess(response.data));
            dispatch(fetchTask());
        } catch (e) {
            dispatch(deleteError(e));
        }
    }
};