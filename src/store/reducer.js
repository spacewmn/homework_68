import {FETCH_ERROR, FETCH_REQUEST, FETCH_SUCCESS, POST_ERROR} from "./actionTypes";

const initialState = {
    toDo: "walk 3 hour",
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_REQUEST:
            return {...state}
        case FETCH_SUCCESS:
            return {...state, toDo: action.task};
        case FETCH_ERROR:
            return {...state, error: action.error}
        case POST_ERROR:
            return {...state, error: action.error}
        default:
            return state;
    }
};

export default reducer;